const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().functions);

var newData;

exports.myTrigger = functions.firestore.document('Notifications/{Notifications}').onCreate(async (snapshot, context) => {
    //

    if (snapshot.empty) {
        console.log('No Devices');
        return;
    }

    newData = snapshot.data();
    var payload = {
        notification: {
            title: newData.title,
            body: newData.messageBody,
            sound: 'default',
        },
        data: {
            push_key: 'Push Key Value',
            message:newData.messageBody,
            title:newData.title,
            //key1: newData.data,
        },
    };

    try {
        const response =await admin.messaging().sendToTopic(newData.topicName,payload);
        console.log('Notification sent successfully');
    } catch (err) {
        console.log(err);
    }
});
var newUser;
exports.newUserTriggerNotification = functions.firestore.document('Users/{Users}').onCreate(async (snapshot, context) => {
    //

    if (snapshot.empty) {
        console.log('No Devices');
        return;
    }

    newUser = snapshot.data();



    var tokens = ['egFmIrRyTuc:APA91bHp8f770wKtAWq2dOnfcBonXLfQFrYezYxkGrwGelCI_SfghjKKDrDMK7YwN2xeDswwVJkej6JUx5AdXof31oEiIP-ziGgZlHoM5nr_TvK1rv5GD6zEc4mk1KpfbvaxwxWHw6lU','dmzign9xPnI:APA91bGOgTqjI5f3OaL-NjY3Z7_JQ0kKMQcvp2c5ab5zE4x65YgARaEKijaCuDVM8EHdbqigrA4Gc4YjsmNqNzSw_q4oHIXBnZqxRm7G5Zt2u-ubDtlzg2RzEPs4UufI0qUZyzQBVaMg'];


    var payload = {
        notification: {
            title: newUser.name,
            body: newUser.email,
            sound: 'default',
        },
        data: {
            push_key: 'Push Key Value',
            message:newUser.phoneNumber,
            title:newUser.email,
            //key1: newData.data,
        },
    };

    try {
       const response = await admin.messaging().sendToDevice(tokens, payload);
        console.log('Notification sent successfully');
    } catch (err) {
        console.log(err);
    }
});