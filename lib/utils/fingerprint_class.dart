import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FingerprintAuth {
  final LocalAuthentication _auth = LocalAuthentication();

  Future<bool> authenticate() async {
    try {
      return await _auth.authenticateWithBiometrics(
          localizedReason: 'Scan your fingerprint to authenticate',
          useErrorDialogs: true,
          stickyAuth: true);
    } on PlatformException catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> checkBiometrics() async {
    try {
      return await _auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
      return false;
    }
  }

  Future<List<BiometricType>> getAvailableBiometrics() async {
    try {
      return await _auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
      return [];
    }
  }
  void cancelAuthentication() {
    _auth.stopAuthentication();
  }
}

class StoreVariable {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<bool> getAuthData() async {
    final SharedPreferences prefs = await _prefs;
    return prefs.getBool('isFingerprintAuth') ?? false;
  }

  Future<void> addAuthData(bool data) async {
    final SharedPreferences prefs = await _prefs;
    prefs.setBool('isFingerprintAuth', data);
  }
}
