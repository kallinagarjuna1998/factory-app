import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/envelope/failure.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:graniteclan/router/router.gr.dart';
import 'package:graniteclan/super_user/providers/recent_user_list_provider.dart';
import 'package:graniteclan/super_user/screens/user_list_tile.dart';
import 'package:graniteclan/widgets/icon_navigation_drawer.dart';
import 'package:graniteclan/widgets/loading_widget.dart';
import 'package:graniteclan/widgets/navigation_drawer.dart';
import 'package:graniteclan/widgets/notification_button.dart';
import 'package:provider/provider.dart';

class SuperUserScreen extends StatefulWidget {
  User user;

  SuperUserScreen({@required this.user});

  @override
  _SuperUserScreenState createState() => _SuperUserScreenState();
}

class _SuperUserScreenState extends State<SuperUserScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();

  @override
  void initState() {
    super.initState();

    //   _getToken();
    _fcm.configure(onMessage: (Map<String, dynamic> message) async {
      print("onMessage:$message");
      final snackBar = SnackBar(
        content: Text(message["notification"]["title"]),
        action: SnackBarAction(
          label: "Go",
          onPressed: () => null,
        ),
      );
      return Scaffold.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(snackBar);
    }, onResume: (Map<String, dynamic> message) async {
      print("onResume:$message");
      ExtendedNavigator.of(context).pushNamed(Routes.notificationsPage);
      final snackBar = SnackBar(
        content: Text(message["notification"]["title"]),
        action: SnackBarAction(
          label: "Go",
          onPressed: () => null,
        ),
      );
    }, onLaunch: (Map<String, dynamic> message) async {
      print("onLaunch:$message");
      final snackBar = SnackBar(
        content: Text(message["notification"]["title"]),
        action: SnackBarAction(
          label: "Go",
          onPressed: () => null,
        ),
      );
      return snackBar;
    });
  }

  _getToken() async {
    _fcm.subscribeToTopic("AllUsers");
    /* String nag = await _fcm.getToken();
    UserService userService = UserService();
    userService.addUser(widget.user.copyWith(fcmToken: nag));*/
  }

  Widget _buildScreen(BuildContext context) {
    //  _fcm.subscribeToTopic("All Users");
    final userProvider = Provider.of<UserProvider>(context);
    final user = userProvider.user;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Super User Screen"),
        leading: IconNavigationDrawer(
          scaffoldKey: _scaffoldKey,
        ),
        actions: <Widget>[
          NotificationButton(),
        ],
      ),
      drawer: NavigationDrawer(),
      body: ChangeNotifierProvider<RecentUserListProvider>(
        create: (context) => RecentUserListProvider(
          userService: RepositoryProvider.of(context),
        ),
        child: Consumer<RecentUserListProvider>(
            builder: (_, userListProvider, __) {
          return _loadUserList(userListProvider);
        }),
      ),
    );
  }

  Widget _loadUserList(RecentUserListProvider userListProvider) {
    return userListProvider.userList.when(
        uninitialized: _buildUnintializedWidget,
        loading: _buildLoadingWidget,
        loaded: _buildLoadedWidget,
        loadedEmpty: _buildNotloadedWidget,
        error: _buildErrorWidgetForTrainer);
  }

  Widget _buildUnintializedWidget() {
    return Text("not intialized");
  }

  Widget _buildNotloadedWidget() {
    return Text("It is Empty");
  }

  Widget _buildErrorWidgetForTrainer(Failure failure) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            failure.message,
            style: TextStyle(fontSize: 20),
          ),
        ],
      ),
    );
  }

  Widget _buildLoadedWidget(UserList userlist) {
    return UserListTile(
      userList: userlist,
      onTap: (user) {
        ExtendedNavigator.of(context).pushNamed(Routes.userDetailsScreen,
            arguments: UserDetailsScreenArguments(
              user: user,
            ));
      },
    );
  }

  Widget _buildLoadingWidget() {
    return LoadingWidget();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<UserProvider>(
      create: (_) => UserProvider(
        appBloc: BlocProvider.of<AppBloc>(context),
        userService: RepositoryProvider.of(context),
        user: widget.user,
      ),
      child: Builder(
        builder: (context) => _buildScreen(context),
      ),
    );
  }
}
