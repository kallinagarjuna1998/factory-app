import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/widgets/circle_text_avatar.dart';


class UserListTile extends StatelessWidget {
  UserList userList;
  ValueSetter<User> onTap;

  UserListTile({@required this.userList, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    final list = userList.list;

    return ListView.builder(
        shrinkWrap: true,
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index) {
          User user = list[index];
          return ListTile(
            title: Text(user.name),
            subtitle: Text(user.email),
            /*   trailing: RawMaterialButton(
              onPressed: () {
                showModalBottomSheet(
                    context: (context),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    builder: (context) {
                      return Container(
                          padding: const EdgeInsets.all(10),
                          child: Wrap(
                            children: <Widget>[
                              ListTile(
                                leading: Icon(Icons.looks_one),
                                title: Text("Normal Call"),
                                onTap: () {
                                  Navigator.pop(context);
                                  videoBloc.add(VideoEvent.startCall(user));
                                },
                              ),
                              ListTile(
                                leading: Icon(Icons.looks_two),
                                title: Text("BroadCastCall"),
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              HostCallScreen(
                                                muted: false,
                                                disableVideo: false,
                                              )));
                                },
                              )
                            ],
                          ));
                    });
              },
              child: Icon(
                Icons.call,
                color: Colors.white,
              ),
              shape: CircleBorder(),
              fillColor: Colors.green,
            ),*/
            leading: SizedBox(
              height: 50,
              width: 50,
              child: CachedNetworkImage(
                  imageUrl: "${user.photoUrl}",
                  imageBuilder: (context, imageProvider) => CircleAvatar(
                        backgroundImage: imageProvider,
                        radius: 30,
                      ),
                  errorWidget: (context, url, error) => CircularTextAvatar(
                        name: user.name,
                      )),
            ),
            onTap: () {
              onTap(user);
            },
          );
        });
  }
}
