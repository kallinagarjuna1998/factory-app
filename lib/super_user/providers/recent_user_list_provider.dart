import 'dart:async';
import 'package:flutter/material.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/services/user_service.dart';
import 'package:graniteclan/super_user/providers/user_list_provider.dart';

class RecentUserListProvider extends UserListProvider {
  RecentUserListProvider({@required UserService userService})
      : super(userService: userService);

  @override
  Stream<UserList> getTraineeListStream() {
    return userService.getAllUsersByLoginTime();
  }
}
