import 'dart:async';

import 'package:flutter/material.dart';
import 'package:graniteclan/envelope/failure.dart';
import 'package:graniteclan/envelope/live_model.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/services/user_service.dart';

abstract class UserListProvider with ChangeNotifier {
  final UserService userService;
  LiveModel<UserList> userList;

  StreamSubscription _userListStreamSubscription;

  UserListProvider({this.userService}) {
    userList = LiveModel<UserList>.uninitialized();
    _loadUsers();
  }

  void _loadUsers() {
    if (_userListStreamSubscription == null) {
      if (userList == LiveModel<UserList>.uninitialized()) {
        userList = LiveModel<UserList>.loading();
        _userListStreamSubscription =
            getTraineeListStream().listen((newUserList) {
          updateUserList(newUserList);
        })
              ..onError((e) {
                userList =
                    LiveModel<UserList>.error(Failure(message: e.toString()));
                notifyListeners();
              });
      }
    }
  }

  Stream<UserList> getTraineeListStream();

  void updateUserList(UserList newUserlist) {
    if (newUserlist == null) {
      userList =
          LiveModel<UserList>.error(Failure(message: "User Data Not Found"));
    } else if (newUserlist.isEmpty) {
      userList = LiveModel<UserList>.loadedEmpty();
    } else
      userList = LiveModel<UserList>.loaded(newUserlist);
    notifyListeners();
  }

  @override
  void dispose() {
    _userListStreamSubscription?.cancel();
    super.dispose();
  }
}
