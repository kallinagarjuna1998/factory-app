import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:graniteclan/providers/user_provider.dart';

import 'package:provider/provider.dart';

import 'circle_text_avatar.dart';

class IconNavigationDrawer extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  IconNavigationDrawer({@required this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    return IconButton(
      iconSize: 50,
        icon: CachedNetworkImage(
            imageUrl: "${userProvider.user.photoUrl}",
            imageBuilder: (context, imageProvider) => CircleAvatar(
                  backgroundImage: imageProvider,
                  radius: 40,
                ),
            placeholder: (context, url) =>
                CircularTextAvatar(name: userProvider.user.name),
            errorWidget: (context, url, error) => CircularTextAvatar(
                  name: userProvider.user.name,
                )),
        onPressed: () {
          scaffoldKey.currentState.openDrawer();
        });
  }
}
