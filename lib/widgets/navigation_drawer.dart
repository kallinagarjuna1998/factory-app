import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/providers/auth_provider.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:graniteclan/router/router.gr.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'circle_text_avatar.dart';

class NavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    final user = userProvider.user;
    return Drawer(
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(user.name),
            accountEmail: Text(user.email),
            onDetailsPressed: () {
              Navigator.pop(context);
              ExtendedNavigator.of(context).pushNamed(Routes.userDetailsScreen,
                  arguments: UserDetailsScreenArguments(
                    user: user,
                    toShowEndDrawer: false,
                  ));
            },
            currentAccountPicture: CircleAvatar(
              backgroundColor: Theme
                  .of(context)
                  .platform == TargetPlatform.iOS
                  ? Colors.blue
                  : Colors.white,
              child: CachedNetworkImage(
                imageUrl: user.photoUrl,
                imageBuilder: (context, imageProvider) =>
                    CircleAvatar(
                      backgroundImage: imageProvider,
                      radius: 50,
                    ),
                placeholder: (context, url) =>
                    CircularTextAvatar(name: user.name),
                errorWidget: (context, url, error) =>
                    CircularTextAvatar(name: user.name),
              ),
            ),
          ),
          Divider(height: 0.5, color: Colors.black26),
          Wrap(children: <Widget>[
            SingleListItem(
              icon: MdiIcons.account,
              onTap: () {
                ExtendedNavigator.of(context).pushNamed(Routes.editUserDetails,
                    arguments: EditUserDetailsArguments(
                      userProvider: userProvider,
                    ));
              },
              text: 'My Profile',
            ),
            /*  ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
              leading: Icon(
                MdiIcons.account,
                color: Colors.black,
              ),
              title: Text(
                'My Profile',
                style: Theme.of(context)
                    .textTheme
                    .body1
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              onTap: () {
                */ /* Navigator.pop(context);
                ExtendedNavigator.of(context).pushNamed(Routes.editProfilePage,
                    arguments: EditUserDetailsArguments(
                      userProvider: userProvider,
                    ));*/ /*
              },
            ),*/

            /*Divider(height: 0.5, color: Colors.black26),
            ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                title: Text('My Qr Code'),
                leading: Icon(
                  FinalIcons.qr_code,
                  color: Colors.black,
                ),
                onTap: () {
                  Navigator.pop(context);
                  ScanType data = new ScanType(type: "3", user: user);
                  ExtendedNavigator.ofRouter<Router>()
                      .pushNamed(Routes.traineeQRScreen,
                          arguments: TraineeQRScreenArguments(
                            user: user,
                            passData: json.encode(data.toJson()),
                          ));
                }),*/
            /* Divider(height: 0.5, color: Colors.black26),
            ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
                title: Text('Scan Log'),
                leading: Icon(
                  FinalIcons.scan,
                  color: Colors.black,
                ),
                onTap: () {
                  Navigator.pop(context);
                  ExtendedNavigator.of(context)
                      .pushNamed(Routes.userScanLogScreen,
                          arguments: UserScanLogScreenArguments(
                            userId: userProvider.user.uid,
                          ));
                }),*/
            Divider(height: 0.5, color: Colors.black26),
            if (user.hasMultipleRoles)
              Column(
                children: [
                  SingleListItem(
                    text: "Switch Roles",
                    icon: MdiIcons.accountSwitchOutline,
                    onTap: () {
                      Navigator.pop(context);
                      BlocProvider.of<AppBloc>(context).add(
                        AppEvent.goToSwitchRoleScreen(userProvider.user),
                      );
                    },
                  ),
                ],
              ),
          ]),
          SingleListItem(
              text: 'Rate this app',
              onTap: () {
                /* var launch = LaunchReview.launch(
                  androidAppId: 'com.gymclan',
                  iOSAppId: '1437050082',
                  writeReview: true);*/
              },
              icon: MdiIcons.radioTower),
          SingleListItem(
              text: 'About Us',
              onTap: () {
                ExtendedNavigator.of(context).pushNamed(Routes.aboutUsPage);
                /* var launch = LaunchReview.launch(
                  androidAppId: 'com.gymclan',
                  iOSAppId: '1437050082',
                  writeReview: true);*/
              },
              icon: MdiIcons.abugidaDevanagari),
          Expanded(child: Container()),
          Divider(height: 0.5, color: Colors.black26),
          ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
            title: Text(
              'Sign Out',
              style: Theme
                  .of(context)
                  .textTheme
                  .body1
                  .copyWith(fontWeight: FontWeight.bold),
            ),
            leading: Icon(
              MdiIcons.logout,
              color: Colors.black,
            ),
            onTap: () {
              Navigator.pop(context);
              Provider.of<AuthProvider>(context, listen: false).signOut();
            },
          ),
          Divider(height: 0.5, color: Colors.black26),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}

class SingleListItem extends StatelessWidget {
  String text;
  Function onTap;
  IconData icon;

  SingleListItem(
      {@required this.text, @required this.onTap, @required this.icon});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
          leading: Icon(this.icon, color: Colors.black),
          trailing: Icon(
            Icons.arrow_right,
            size: 30,
            color: Colors.black,
          ),
          title: Text(this.text,
              style: TextStyle(color: Colors.black, fontSize: 15.0)),
          onTap: this.onTap,
        ),
        Divider(height: 0.5, color: Colors.black26)
      ],
    );
  }
}

