import 'package:flutter/material.dart';

class CenterWidget extends StatelessWidget {
  String text;
  IconData icon;

  CenterWidget({this.text, this.icon});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 120,
            height: 120,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white30,
              ),
              child: Icon(
                icon,
                color: Colors.white,
                size: 80,
              ),
            ),
          ),

          /* Image.asset(
            'images/logo.png',
            width: 130,
            height: 130,
          ),*/

          SizedBox(
            height: 20,
          ),
          Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 18),
          )
        ],
      ),
    );
  }
}
