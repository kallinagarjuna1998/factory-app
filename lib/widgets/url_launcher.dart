import 'package:url_launcher/url_launcher.dart';

class UrlLauncher {
  Future<void> call(String number) async {
    final url = 'tel:${number}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> whatsApp(String number, {String message}) async {
     if (!number.startsWith('+')){
       number = '+91$number';
     }
     number = number.replaceFirst('+', '');
    final url = 'https://wa.me/$number';
    if (await canLaunch(url)) {
      await launch(url);
    } else{
      throw 'Could not launch $url';
    }
  }

  Future<void> email(String email) async {
    final url = 'mailto:${email}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _launchPlayStoreURL() async {
    const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
