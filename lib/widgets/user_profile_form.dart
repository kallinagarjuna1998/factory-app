import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/widgets/diet_ondone_button.dart';


class UserProfileForm extends StatefulWidget {
  final User user;
  final bool isEdit;
  final ValueSetter<User> onSaveCallback;

  @override
  _UserProfileFormState createState() => _UserProfileFormState();

  UserProfileForm(
      {Key key,
      @required this.user,
      @required this.isEdit,
      @required this.onSaveCallback})
      : super(key: key);
}

class _UserProfileFormState extends State<UserProfileForm> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  Future<void> _validateAndSave() async {
    if (_fbKey.currentState.saveAndValidate()) {
      print(_fbKey.currentState.value);
      User entry = User.fromJson(_fbKey.currentState.value);
      widget.onSaveCallback(entry);
    }
  }

  Widget build(BuildContext context) {
    final user = widget.user;
    return Padding(
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            FormBuilder(
                key: _fbKey,
                autovalidate: true,
                readOnly: widget.isEdit,
                initialValue: user.toJson(),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: FormBuilderTextField(
                        attribute: "name",
                        decoration: InputDecoration(
                            hintText: "Your full name", labelText: "Name"),
                        //controller: _name,
                        validators: [FormBuilderValidators.required()],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: FormBuilderTextField(
                        attribute: "email",
                        readOnly: !widget.isEdit,
                        decoration: InputDecoration(
                            hintText: "youremail@example.com",
                            labelText: "Email"),
                        //controller: _email,
                        validators: [
                          FormBuilderValidators.required(),
                          FormBuilderValidators.email(),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: FormBuilderTextField(
                        attribute: "age",
                        decoration: InputDecoration(labelText: "Age"),
                        valueTransformer: (text) => num.tryParse(text),
                        initialValue: user.age?.toString(),
                        validators: [
                          FormBuilderValidators.required(),
                          FormBuilderValidators.numeric(),
                          FormBuilderValidators.min(10),
                          FormBuilderValidators.max(80),
                        ],
                        keyboardType: TextInputType.number,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: FormBuilderPhoneField(
                        attribute: 'phoneNumber',
                        //initialValue: "+91",
                        defaultSelectedCountryIsoCode: 'IN',
                        cursorColor: Colors.black,
                        // style: TextStyle(color: Colors.black, fontSize: 18),
                        decoration: InputDecoration(
                          //border: OutlineInputBorder(),
                          labelText: "Phone Number",
                        ),
                        //onChanged: _onChanged,
                        //priorityListByIsoCode: ['IN'],
                        validators: [
                          FormBuilderValidators.required(),// FormBuilderValidators.required(),
                          FormBuilderValidators.numeric(),
                          FormBuilderValidators.maxLength(13),
                          FormBuilderValidators.minLength(12),

                        ],
                      ),

                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: FormBuilderChoiceChip(
                        attribute: "gender",
                        validators: [
                          FormBuilderValidators.required(),
                        ],
                        decoration: InputDecoration(
                          labelText: "Gender",
                        ),
                        options: [
                          FormBuilderFieldOption(
                            value: 1,
                            child: Text("Male"),
                          ),
                          FormBuilderFieldOption(
                            value: 2,
                            child: Text("Female"),
                          )
                        ],
                        onChanged: (value) {
                          print(value);
                        },
                      ),
                    ),
                  ],
                )),
            OnDoneButton(
              text: "Save",
              onPressed: () {
                _validateAndSave();
              },
            )
          ],
        ),
      ),
    );
  }
}
