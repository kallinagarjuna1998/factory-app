import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:graniteclan/router/router.gr.dart';

class NotificationButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        //  Provider.of<AuthProvider>(context, listen: false).signOut();
        ExtendedNavigator.of(context).pushNamed(Routes.notificationsPage);
      },
      icon: Icon(Icons.notifications_active),
    );
  }
}
