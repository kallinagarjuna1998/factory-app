import 'package:flutter/material.dart';

class CircularTextAvatar extends StatelessWidget {
  final String name;
  final double radius;

  CircularTextAvatar({@required this.name, this.radius : 50});

  @override
  Widget build(BuildContext context) {
    String msg;

    msg = name.substring(0, 1);

    return CircleAvatar(
      backgroundColor: Theme.of(context).primaryColor,
      radius: this.radius,
      child: Text(
        msg,
        style: TextStyle(
          color: Colors.white,
          fontSize: 30,
        ),
      ),
    );
  }
}
