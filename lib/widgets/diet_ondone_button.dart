import 'package:flutter/material.dart';

class FAB extends StatelessWidget {
  Function onPressed;
  IconData iconData;

  //String text;

  FAB({@required this.onPressed, @required this.iconData});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onPressed,
      backgroundColor: Theme.of(context).primaryColor,
      child: Icon(
        iconData,
        color: Colors.white,
        size: 28,
      ),
    );
  }
}

class OnDoneButton extends StatelessWidget {
  final Function onPressed;
  final String text;

  OnDoneButton({this.onPressed, this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 4),
      child: SizedBox(
        width: double.infinity,
        height: 40,
        child: RaisedButton(
          onPressed: onPressed,
          shape: StadiumBorder(),
          color: Theme.of(context).accentColor,
          child: Text(
            text,
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
        ),
      ),
    );
  }
}

class OnDoneIconButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final Icon icon;

  OnDoneIconButton({this.onPressed, this.text, this.icon});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: SizedBox(
        width: double.infinity,
        height: 40,
        child: RaisedButton.icon(
          onPressed: onPressed,
          shape: StadiumBorder(),
          color: Theme.of(context).accentColor,
          icon: icon,
          label: Text(
            text,
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
        ),
      ),
    );
  }
}
