import 'package:flui/flui.dart';
import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: ScrollPhysics(),
      itemCount: 8,
      shrinkWrap: true,
      itemBuilder: (BuildContext context,int index){
        return  Container(
            padding: EdgeInsets.all(10),
            child: Card(
              child: Stack(
                children: <Widget>[
                  FLSkeleton(
                    shape: BoxShape.circle,
                    margin: EdgeInsets.only(top: 10, left: 10),
                    width: 40,
                    height: 40,
                  ),
                  FLSkeleton(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(2),
                    margin: EdgeInsets.only(left: 60, top: 10, right: 10),
                    height: 20,
                  ),
                  FLSkeleton(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(2),
                    margin: EdgeInsets.only(left: 60, top: 40),
                    width: 300,
                    height: 20,
                  ),
                  FLSkeleton(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(2),
                    margin: EdgeInsets.only(left: 60, top: 70, bottom: 10),
                    width: 100,
                    height: 20,
                  ),
                ],
              ),
            )
        );
      },

    );
  }
}
