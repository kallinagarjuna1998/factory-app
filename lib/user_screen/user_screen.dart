import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/screens/fingerprint_screen.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:graniteclan/screens/Home.dart';
import 'package:graniteclan/status/status.dart';
import 'package:graniteclan/widgets/icon_navigation_drawer.dart';
import 'package:graniteclan/widgets/navigation_drawer.dart';
import 'package:graniteclan/widgets/notification_button.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

class UserScreen extends StatefulWidget {
  User user;

  UserScreen({@required this.user});

  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[
    FirstTab(),
    Status(),
    FingerprintScreen(),
  ];

  Widget _buildScreen2(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("To Do App"),
        leading: IconNavigationDrawer(
          scaffoldKey: _scaffoldKey,
        ),
        actions: <Widget>[
          NotificationButton(),
        ],
      ),
      drawer: NavigationDrawer(),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1)),
            ],
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15.0,
              vertical: 8,
            ),
            child: GNav(
                gap: 8,
                activeColor: Colors.white,
                iconSize: 24,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: Duration(milliseconds: 800),
                tabBackgroundColor: Theme.of(context).primaryColor,
                tabs: [
                  GButton(
                    icon: MdiIcons.home,
                    text: 'Home',
                  ),
                  GButton(
                    icon: MdiIcons.stateMachine,
                    text: 'Status',
                  ),
                  GButton(
                    icon: Icons.settings,
                    text: 'Settings',
                  ),
                ],
                selectedIndex: _selectedIndex,
                onTabChange: (index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                }),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<UserProvider>(
      create: (_) => UserProvider(
        appBloc: BlocProvider.of<AppBloc>(context),
        userService: RepositoryProvider.of(context),
        user: widget.user,
      ),
      child: Builder(
        builder: (context) => _buildScreen2(context),
      ),
    );
  }
}
