import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:graniteclan/fcm_notifications/models/notification.dart';
import 'package:graniteclan/models/user.dart';

extension UserExtension on FirebaseUser {
  User asUser() {
    return User(
      uid: this.uid,
      name: this.displayName,
      email: this.email,
      phoneNumber: this.phoneNumber,
      photoUrl: this.photoUrl,
    );
  }
}

extension ModelExtension on DocumentSnapshot {
  User asUser() {
    if (!this.exists) return null;

    final data = this.data;

    final startDateTimeStamp = data['registeredTime'];
    if (startDateTimeStamp is Timestamp) {
      data['registeredTime'] = startDateTimeStamp.toDate().toIso8601String();
    }

    return User.fromJson(data).copyWith(
      uid: this.documentID,
    );
  }

  FcmNotification asFcmNotification() {
    if (!this.exists) return null;

    final data = this.data;

    return FcmNotification.fromJson(data).copyWith(
      id: this.documentID,
    );
  }
}
