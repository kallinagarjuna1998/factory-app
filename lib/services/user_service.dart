import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:graniteclan/models/user.dart';

import 'firebase_model_extensions.dart';

class UserService {
  Firestore _firestore;
  static const keyUsers = "Users",
      keyUid = 'uid',
      keyPhoneNumber = "phoneNumber",
      keyEmail = "email",
      keyTrainerId = "trainerId",
      keyLoginTime = "registeredTime";

  UserService() : _firestore = Firestore.instance;

  Stream<UserList> getAllUsers() {
    return _firestore
        .collection(keyUsers)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      final List<User> userList = snapshot.documents
          .map((DocumentSnapshot document) => document.asUser())
          .toList();

      return UserList(userList);
    });
  }

  Stream<UserList> getAllAdminRoleUsers() {
    return _firestore
        .collection(keyUsers)
        .where("isAdmin", isEqualTo: true)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      final List<User> userList = snapshot.documents
          .map((DocumentSnapshot document) => document.asUser())
          .toList();

      return UserList(userList);
    });
  }

  Stream<UserList> getAllUsersByLoginTime() {
    return _firestore
        .collection(keyUsers)
        .orderBy(keyLoginTime, descending: true)
        .limit(100)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      final List<User> userList = snapshot.documents
          .map((DocumentSnapshot document) => document.asUser())
          .toList();

      return UserList(userList);
    });
  }

  Stream<UserList> getUsersByTrainer(String uid) {
    return _firestore
        .collection(keyUsers)
        .where(keyTrainerId, isEqualTo: uid)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      final List<User> userList = snapshot.documents
          .map((DocumentSnapshot document) => document.asUser())
          .toList();

      return UserList(userList);
    });
  }

  Stream<UserList> getUsersListByTrainer(String uid) {
    return _firestore
        .collection(keyUsers)
        .where('trainerId', isEqualTo: uid)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      final List<User> userList = snapshot.documents
          .map((DocumentSnapshot document) => document.asUser())
          .toList();

      return UserList(userList);
    });
  }

  Stream<UserList> getUserByPhoneNumber(String no) {
    return _firestore
        .collection(keyUsers)
        .where(keyPhoneNumber, isEqualTo: no)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      final List<User> userList = snapshot.documents
          .map((DocumentSnapshot document) => document.asUser())
          .toList();
      return UserList(userList);
    });
  }

  Stream<UserList> getUserByEmail(String mail) {
    return _firestore
        .collection(keyUsers)
        .where(keyEmail, isEqualTo: mail)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      final List<User> userList = snapshot.documents
          .map((DocumentSnapshot document) => document.asUser())
          .toList();
      return UserList(userList);
    });
  }

  Future<UserList> getListByPhoneNumber(String no) async {
    QuerySnapshot snapshot = await _firestore
        .collection(keyUsers)
        .where(keyPhoneNumber, isEqualTo: no)
        .getDocuments();
    final List<User> userList = snapshot.documents
        .map((DocumentSnapshot document) => document?.asUser())
        .toList();
    return UserList(userList);
  }

  Future<UserList> getListByEmail(String email) async {
    QuerySnapshot snapshot = await _firestore
        .collection(keyUsers)
        .where(keyEmail, isEqualTo: email)
        .getDocuments();
    final List<User> userList = snapshot.documents
        .map((DocumentSnapshot document) => document?.asUser())
        .toList();
    return UserList(userList);
  }

  Stream<User> getUser(String uid) {
    return _firestore
        .collection(keyUsers)
        .document(uid)
        .snapshots()
        .map((DocumentSnapshot document) {
      final User user = document?.asUser();

      return user;
    });
  }

  Future<bool> updateUser({@required User user, @required String uid}) async {
    final userRef = _firestore.collection(keyUsers).document(uid);
    final data = user.toJson();
    data['registeredTime'] = Timestamp.fromDate(user.registeredTime);
    try {
      await userRef.setData(data, merge: true);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> saveData(
      {@required Map<String, dynamic> data, @required String uid}) async {
    final userRef = _firestore.collection(keyUsers).document(uid);
    try {
      await userRef.setData(data, merge: true);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> addUser(User entry) async {
    final usersRef = _firestore.collection(keyUsers);

    final documentId = entry.uid ?? usersRef.document().documentID;

    final data = entry.toJson();

    data['registeredTime'] = Timestamp.fromDate(entry.registeredTime);

    try {
      await usersRef.document(documentId).setData(data);
      return true;
    } catch (e) {
      return false;
    }

    /* Alternate implementation with if else
    final data = entry.toJson();
    if (entry.uid != null) {
      await _firestore.collection(keyUsers).document(entry.uid).setData(data);
    } else {
      await _firestore.collection(keyUsers).document().setData(data);
    }
    */
  }
}
