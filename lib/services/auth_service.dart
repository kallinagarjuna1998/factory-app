import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:graniteclan/models/user.dart';

import 'firebase_model_extensions.dart';

class AuthService with ChangeNotifier {
  FirebaseAuth _auth;
  GoogleSignIn _googleSignIn;

  AuthService() {
    _auth = FirebaseAuth.instance;
    _googleSignIn = GoogleSignIn();
  }

  Future<User> currentUser() async {
    final currentUser = await _auth.currentUser();

    return currentUser?.asUser();
  }

  Stream<User> get onAuthStateChanged {
    return _auth.onAuthStateChanged.map((FirebaseUser firebaseUser) {
      return firebaseUser?.asUser();
    });
  }

  Future<bool> signIn(String Email, String Password) async {
    try {
      await _auth.signInWithEmailAndPassword(email: Email, password: Password);
      notifyListeners();
      return true;
    } catch (e) {
      notifyListeners();
      return false;
    }
  }

  AuthCredential credential;

  Future<bool> signInWithGoogle() async {
    try {
      notifyListeners();
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
      await googleUser.authentication;
      credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      //  await _auth.signInWithCredential(credential);
      notifyListeners();
      return true;
    } catch (e) {
      print(e);
      notifyListeners();
      return false;
    }
  }

  Future<bool> loggingIn() async {
    try {
      await _auth.signInWithCredential(credential);
      notifyListeners();
      return true;
    } catch (e) {
      notifyListeners();
      return false;
    }
  }

  Future signOut() async {
    _auth.signOut();
    _googleSignIn.signOut();
    notifyListeners();
    return Future.delayed(Duration.zero);
  }
}
