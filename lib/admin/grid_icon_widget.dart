import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class GridIconWidget extends StatelessWidget {
  IconData icon;
  Color color;
  String name;
  VoidCallback onTap;
  Color backgroundColor;

  GridIconWidget(
      {@required this.icon,
      @required this.name,
      @required this.color,
      this.onTap,
      this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 50,
              child: Material(
                color: backgroundColor,
                shape: CircleBorder(),
                child: Icon(
                  icon,
                  color: color,
                  size: 40,
                ),
              ),
            ),
            SizedBox(
              height: 3,
            ),
            Flexible(
              child: AutoSizeText(
                name,
                //style: Theme.of(context).textTheme.body1,
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        onTap();
      },
    );
  }
}
