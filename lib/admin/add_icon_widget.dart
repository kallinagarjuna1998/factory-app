import 'package:flutter/material.dart';

class AddIconWidget extends StatelessWidget {
  String name;
  VoidCallback onPressed;

  AddIconWidget({@required this.name, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Column(
        children: <Widget>[
          Container(
            height: 50,
            decoration:
                BoxDecoration(shape: BoxShape.circle, color: Colors.deepPurple),
            child: Center(
              child: Icon(Icons.add, color: Colors.white),
            ),
          ),
          Text(
            name,
            style: Theme.of(context).textTheme.title,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
