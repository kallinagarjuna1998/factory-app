import 'package:auto_route/auto_route.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/admin/grid_icon_widget.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:graniteclan/router/router.gr.dart';
import 'package:graniteclan/widgets/icon_navigation_drawer.dart';
import 'package:graniteclan/widgets/navigation_drawer.dart';
import 'package:graniteclan/widgets/notification_button.dart';
import 'package:provider/provider.dart';

class AdminScreen extends StatefulWidget {
  final User user;

  AdminScreen({@required this.user});

  @override
  _AdminScreenState createState() => _AdminScreenState();
}

class _AdminScreenState extends State<AdminScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final FirebaseMessaging _fcm = FirebaseMessaging();


  @override
  void initState() {
    super.initState();

    //   _getToken();
    _fcm.configure(onMessage: (Map<String, dynamic> message) async {
      print("onMessage:$message");
      final snackBar = SnackBar(
        content: Text(message["notification"]["title"]),
        action: SnackBarAction(
          label: "Go",
          onPressed: () => null,
        ),
      );
      return Scaffold.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(snackBar);
    }, onResume: (Map<String, dynamic> message) async {
      print("onResume:$message");
      ExtendedNavigator.of(context).pushNamed(Routes.notificationsPage);
    }, onLaunch: (Map<String, dynamic> message) async {
      print("onLaunch:$message");
      final snackBar = SnackBar(
        content: Text(message["notification"]["title"]),
        action: SnackBarAction(
          label: "Go",
          onPressed: () => null,
        ),
      );
      ExtendedNavigator.of(context).pushNamed(Routes.notificationsPage);
    });
  }

  Widget _buildScreen(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    final user = userProvider.user;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Admin Screen"),
        leading: IconNavigationDrawer(
          scaffoldKey: _scaffoldKey,
        ),
        actions: <Widget>[
          NotificationButton(),
        ],
      ),
      drawer: NavigationDrawer(),
      body: CustomScrollView(slivers: <Widget>[
        SliverGrid.count(
          crossAxisCount: 3,
          children: <Widget>[
            GridIconWidget(
              name: 'Single Device Notifications',
              color: Colors.white,
              backgroundColor: Colors.deepPurple,
              icon: Icons.add,
              onTap: () {
                /*ExtendedNavigator.of(context).pushNamed(
                    Routes.addGymSubscriptionDetails,
                    arguments: AddGymSubscriptionDetailsArguments(
                        userProvider: userProvider));*/
              },
            ),
            GridIconWidget(
              name: 'Topic Notifications',
              color: Colors.white,
              backgroundColor: Colors.deepPurple,
              icon: Icons.add,
              onTap: () {
                ExtendedNavigator.of(context).pushNamed(
                    Routes.createTopicNotification,
                    arguments: CreateTopicNotificationArguments(user: user));
              },
            ),
          ],
        ),

//        SliverToBoxAdapter(
//          child: _buildImageCarousel(context),
//        ),

        SliverToBoxAdapter(
          child: Divider(
            color: Theme.of(context).primaryColor,
          ),
        ),

        SliverGrid.count(crossAxisCount: 3, children: [
          GridIconWidget(
            icon: Icons.notifications_active,
            color: Colors.deepPurpleAccent,
            name: 'Single Device Notifications',
            onTap: () {

            },
          ),
          GridIconWidget(
            icon: Icons.notifications_active,
            color: Colors.lightBlue,
            name: 'My Topic Notifications',
            onTap: () {

            },
          ),
          /*
          GridIconWidget(
            icon: MdiIcons.noodles,
            color: Colors.black87,
            name: 'My Trainers',
            onTap: () {
              *//*ExtendedNavigator.of(context).pushNamed(Routes.trainerListScreen,
                  arguments: TrainerListScreenArguments(
                    userProvider: userProvider,

                  ));*//*
            },
          ),
          GridIconWidget(
            icon: MdiIcons.link,
            color: Colors.red,
            name: 'UnLinked Subscriptions ',
            onTap: () {
              *//* ExtendedNavigator.of(context)
                  .pushNamed(Routes.unLinkedSubscriptionListScreen,
                  arguments: UnLinkedSubscriptionListScreenArguments(
                    userProvider: userProvider,
                  ));*//*
            },
          ),
          GridIconWidget(
            icon: MdiIcons.tab,
            color: Colors.green,
            name: 'Active Subscriptions',
            onTap: () {
              *//*  ExtendedNavigator.of(context)
                  .pushNamed(Routes.activeGymSubscriptionListScreen,
                  arguments: ActiveGymSubscriptionListScreenArguments(
                    userProvider: userProvider,
                  ));*//*
            },
          ),*/
//
        ]),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<UserProvider>(
      create: (_) => UserProvider(
        appBloc: BlocProvider.of<AppBloc>(context),
        userService: RepositoryProvider.of(context),
        user: widget.user,
      ),
      child: Builder(
        builder: (context) => _buildScreen(context),
      ),
    );
  }
}
