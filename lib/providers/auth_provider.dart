import 'package:flutter/material.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/services/auth_service.dart';

class AuthProvider with ChangeNotifier {
  final AppBloc appBloc;
  final AuthService authService;

  User currentUser;

  AuthProvider({
    @required this.appBloc,
    @required this.authService,
  }) {
    authService.onAuthStateChanged.listen((User newUser) {
      currentUser = newUser;
      notifyListeners();

      if (isAuthenticated) {
        appBloc.add(AppEvent.authenticated(currentUser));
      } else {
        appBloc.add(AppEvent.notAuthenticated());
      }
    });
  }

  bool get isAuthenticated => currentUser != null;

  void signInWithGoogle() async {
    appBloc.add(AppEvent.loading());
    if (!await authService.signInWithGoogle()) {
      appBloc.add(AppEvent.error("Login was cancelled"));
      appBloc.add(AppEvent.notAuthenticated());
    } else {
      appBloc.add(AppEvent.logging());
      if (!await authService.loggingIn()) {
        appBloc.add(AppEvent.error("Login Failed"));
        appBloc.add(AppEvent.notAuthenticated());
      }
    }
  }

  Future<void> signOut() async {
    return authService.signOut();
  }
}
