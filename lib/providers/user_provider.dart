import 'dart:async';
import 'package:flutter/material.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/services/user_service.dart';

class UserProvider with ChangeNotifier {
  UserService userService;

  AppBloc appBloc;
  User user;

  StreamSubscription _userStreamSubscription;

  String userId;

  UserProvider({
    @required this.userService,
    this.user,
    this.userId,
    @required this.appBloc,
  }) : assert(user != null || userId != null,
            "Either User or userId is required") {
    if (user != null) {
      userId = user.uid;
    }

    getUser();
  }

  getUser() {
    if (_userStreamSubscription == null) {
      _userStreamSubscription = userService.getUser(userId).listen((newUser) {
        _updateUser(newUser);
        if (appBloc != null) appBloc.add(AppEvent.checkAndSwitchAccount(user));
      })
        ..onError((error) {
          print(error);
        });
    }
  }

  _updateUser(User newUser) {
    user = newUser;
    notifyListeners();
  }

  void addUser(User entry) {
    userService.addUser(entry);
  }

  void updateUser({@required User user, @required String uid}) {
    userService.updateUser(user: user, uid: uid);
  }

  void _saveData({@required Map<String, dynamic> data}) {
    userService.saveData(data: data, uid: user.uid);
  }

  void updateRoleData({@required Map<String, dynamic> data}) {
    _saveData(data: data);
  }

  @override
  void dispose() {
    _userStreamSubscription?.cancel();

    //  _trainerStreamSubscription2.cancel();
    super.dispose();
  }
}
