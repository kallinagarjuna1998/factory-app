import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

part 'user.g.dart';

@freezed
abstract class User with _$User {
  factory User({
    @required uid,
    String name,
    String email,
    String phoneNumber,
    String fcmToken,
    int age,
    int gender,
    String photoUrl,
    bool isUser,
    bool isAdmin,
    bool isSU,
    DateTime registeredTime,
  }) = _User;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  @late
  Map<String, dynamic> get roleData => <String, dynamic>{
        'isUser': isUser,
        "isAdmin": isAdmin,
        "isSU": isSU,
      };

  @late
  bool get hasMultipleRoles => _checkMultipleRoles();
}

@freezed
abstract class UserList with _$UserList {
  factory UserList(List<User> list) = _UserList;

  @late
  bool get isEmpty => list.isEmpty;

  @late
  bool get isNotEmpty => list.isNotEmpty;
}

extension _ConcreteMethods on User {
  bool _checkMultipleRoles() {
    int num = 0;
    if (this.isSU ?? false) {
      num++;
    }
    if (this.isAdmin ?? false) {
      num++;
    }
    if (this.isUser ?? false) {
      num++;
    }
    return num >= 2;
  }
}
