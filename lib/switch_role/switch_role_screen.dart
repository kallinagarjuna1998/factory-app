import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:graniteclan/utils/fingerprint_class.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SwitchRoleScreen extends StatefulWidget {
  final User user;

  @override
  SwitchRoleScreen({@required this.user});

  @override
  _SwitchRoleScreenState createState() => _SwitchRoleScreenState();
}

class _SwitchRoleScreenState extends State<SwitchRoleScreen> {
  FingerprintAuth fingerprintAuth = FingerprintAuth();

  bool _isFingerPrintAuth = false;

  Widget SwitchRoleListTile(String text, IconData icon, VoidCallback onTap) =>
      ListTile(
          title: Text(
            text,
          ),
          leading: Icon(
            icon,
            size: 50,
            color: Colors.black,
          ),
          trailing: Wrap(
            children: [
              Icon(
                _isFingerPrintAuth ? Icons.lock : Icons.lock_open,
                color: _isFingerPrintAuth ? Colors.red : Colors.green,
              ),
              Icon(
                Icons.arrow_forward,
                size: 30,
                color: _isFingerPrintAuth ? Colors.red : Colors.green,
              ),
            ],
          ),
          onTap: () async => _isFingerPrintAuth
              ? await fingerprintAuth.authenticate() ? onTap() : null
              : onTap());

  @override
  void initState() {
    super.initState();

    _loadCounter();
  }

  _loadCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _isFingerPrintAuth = (prefs.getBool('isFingerprintAuth') ?? false);
  }

  Widget _buildScreen(BuildContext context) {
    final user = Provider.of<UserProvider>(context).user;
    final appBloc = BlocProvider.of<AppBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Switch Role"),
      ),
      body: user.hasMultipleRoles
          ? Column(children: <Widget>[
              if (user.isUser)
                SwitchRoleListTile("User", MdiIcons.alphaU,
                    () => appBloc.add(AppEvent.viewAsUser(user))),
              if (user.isSU)
                SwitchRoleListTile("Super User", MdiIcons.alphaS,
                    () => appBloc.add(AppEvent.viewAsSuperUser(user))),
              if (user.isAdmin)
                SwitchRoleListTile("Admin", MdiIcons.alphaA,
                    () => appBloc.add(AppEvent.viewAsAdmin(user))),
            ])
          : Center(
              child: Text("No Row's To Switch"),
            ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<UserProvider>(
        create: (context) => UserProvider(
              userService: RepositoryProvider.of(context),
              appBloc: BlocProvider.of<AppBloc>(context),
              user: widget.user,
            ),
        child: Builder(
          builder: (context) {
            return _buildScreen(context);
          },
        ));
  }
}
