import 'package:flutter/material.dart';
import 'package:graniteclan/utils/fingerprint_class.dart';
import 'package:local_auth/local_auth.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FingerprintScreen extends StatefulWidget {
  @override
  _FingerprintScreenState createState() => _FingerprintScreenState();
}

class _FingerprintScreenState extends State<FingerprintScreen> {
  final LocalAuthentication auth = LocalAuthentication();
  bool _canCheckBiometrics = false;
  List<BiometricType> _availableBiometrics;

  bool nag;
  StoreVariable storeVariable = StoreVariable();
  FingerprintAuth fingerprintAuth = FingerprintAuth();
  bool _isFingerPrintAuth = false;

  @override
  void initState() {
    super.initState();

    _loadCounter();
  }

  //Loading counter value on start
  _loadCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _canCheckBiometrics = await fingerprintAuth.checkBiometrics();

    setState(() {
      _isFingerPrintAuth = (prefs.getBool('isFingerprintAuth') ?? false);
    });
  }

  @override
  Widget build(BuildContext context) {
    StoreVariable storeVariable = StoreVariable();
    return Scaffold(
      body: ConstrainedBox(
          constraints: const BoxConstraints.expand(),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                if (_canCheckBiometrics ?? false)
                  SwitchListTile(
                    value: _isFingerPrintAuth,
                    title: ListTile(
                      leading: Icon(
                        _isFingerPrintAuth
                            ? MdiIcons.fingerprint
                            : MdiIcons.fingerprintOff,
                        color: _isFingerPrintAuth ? Colors.green : Colors.red,
                        size: 35,
                      ),
                      title: Text("Fingerprint"),
                    ),
                    onChanged: (value) {
                      storeVariable.addAuthData(value);
                      setState(() {
                        _isFingerPrintAuth = !_isFingerPrintAuth;
                      });
                    },
                  ),
                // if()
              ])),
    );
  }
}
