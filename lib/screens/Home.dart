
import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:provider/provider.dart';

class FirstTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    final user = userProvider.user;
    final backgroundColor = Theme.of(context).accentColor;
    final foregroundColor = Colors.white;
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: CarouselSlider(
              items: [
                Card(
                  color: backgroundColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  child: InkWell(
                    onTap: () async {
                      /* var file = await cacheManager.getSingleFile(ad.shareImgUrl);
                  Uint8List bytes = await file.readAsBytes();

                  ShareManager.shareFile(ad.shareTitle, 'fitpaa.png',
                      bytes.buffer.asUint8List(), 'image/png',
                      text: ad.shareDescription);*/

                      // await Share.file(ad.shareTitle, 'fitpaa.png', bytes.buffer.asUint8List(), 'image/png', text: ad.shareDescription);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.zero,
                          title: AutoSizeText(
                            'image slider',
                            maxLines: 2,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle
                                .copyWith(fontSize: 14.0, color: Colors.white),
                          ),
                          leading: CircleAvatar(
                            radius: 20,
                            backgroundColor: foregroundColor,
                            foregroundColor: backgroundColor,
                            child: Icon(
                              Icons.share,
                              size: 24,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Card(
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  child: InkWell(
                    onTap: () async {
                      /* var file = await cacheManager.getSingleFile(ad.shareImgUrl);
                  Uint8List bytes = await file.readAsBytes();

                  ShareManager.shareFile(ad.shareTitle, 'fitpaa.png',
                      bytes.buffer.asUint8List(), 'image/png',
                      text: ad.shareDescription);*/

                      // await Share.file(ad.shareTitle, 'fitpaa.png', bytes.buffer.asUint8List(), 'image/png', text: ad.shareDescription);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.zero,
                          title: AutoSizeText(
                            'image slider',
                            maxLines: 2,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle
                                .copyWith(fontSize: 14.0, color: Colors.white),
                          ),
                          leading: CircleAvatar(
                            radius: 20,
                            backgroundColor: foregroundColor,
                            foregroundColor: backgroundColor,
                            child: Icon(
                              Icons.share,
                              size: 24,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
              aspectRatio: 5,
              viewportFraction: 0.8,
              enlargeCenterPage: false,
              autoPlay: true,
              pauseAutoPlayOnTouch: Duration(seconds: 15),
            ),
          ),
        ],
      ),
    );
  }
}
