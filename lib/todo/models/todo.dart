import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'todo.freezed.dart';

part 'todo.g.dart';

@freezed
abstract class Todo with _$Todo {
  factory Todo({
     String docId,
    String title,
    String description,
    bool status,
    String uid,
  }) = _Todo;

  factory Todo.fromJson(Map<String, dynamic> json) => _$TodoFromJson(json);


}

@freezed
abstract class TodoList with _$TodoList {
  factory TodoList(List<Todo> list) = _TodoList;

  @late
  bool get isEmpty => list.isEmpty;

  @late
  bool get isNotEmpty => list.isNotEmpty;
}