import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/admin/admin_screen.dart';
import 'package:graniteclan/fingerprint/fingerprint_auth_screen.dart';
import 'package:graniteclan/login/login_screen.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/onboarding/bloc/bloc.dart';
import 'package:graniteclan/onboarding/screens/onboarded_builder.dart';
import 'package:graniteclan/super_user/screens/superuser_screen.dart';
import 'package:graniteclan/switch_role/switch_role_screen.dart';
import 'package:graniteclan/user_screen/user_screen.dart';

import 'app_bloc/bloc.dart';

class HomeScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: BlocConsumer<AppBloc, AppState>(
        listener: (context, state) {
          state.maybeWhen(
              showError: (msg) {
                _scaffoldKey.currentState
                  ..hideCurrentSnackBar()
                  ..showSnackBar(SnackBar(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          msg,
                          textAlign: TextAlign.center,
                        ),
                        Icon(Icons.error)
                      ],
                    ),
                    backgroundColor: Colors.red,
                  ));
              },
              orElse: () {});
        },
        builder: (context, state) {
          return state.maybeWhen(
              notAuthenticated: (isLogin, msg) =>
                  LoginScreen(
                    isLogin: isLogin,
                    msg: msg,
                  ),
              authenticated: _buildOnboardingScreen,
              //admin: NewAdminScreen(),
              admin: _buildAdminScreen,
              user: _buildTraineeScreen,
              superUser: _buildSuperUserScreen,
              switchRole: _buildSwitchRoleScreen,
              fingerPrintAuthScreen: _buildFingerPrintScreen,
              orElse: () =>
                  LoginScreen(
                    isLogin: false,
                    msg: "Loading",
                  ));
        },
      ),
    );
  }
}

Widget _buildFingerPrintScreen(User user) {
  return FingerPrintAuthScreen(
    user: user,
  );
}

Widget _buildSwitchRoleScreen(User user) {
  return SwitchRoleScreen(
    user: user,
  );
}

Widget _buildOnboardingScreen(User user) {
  return BlocProvider<OnboardingBloc>(
      create: (context) =>
          OnboardingBloc(
            userService: RepositoryProvider.of(context),
            user: user,
            appBloc: BlocProvider.of<AppBloc>(context),
          ),
      child: OnboardingScreen());
}

Widget _buildAdminScreen(User user) {
  return AdminScreen(
    user: user,
  );
}

Widget _buildTraineeScreen(User user) {
  return UserScreen(
    user: user,
  );
}

Widget _buildTrainerScreen(User user) {
  return Scaffold(
    appBar: AppBar(
      title: Text("Home"),
    ),
    body: Center(
      child: Text("home screen"),
    ),
  );
}

Widget _buildSuperUserScreen(User user) {
  return SuperUserScreen(
    user: user,
  );
}
