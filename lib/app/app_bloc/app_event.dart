import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:graniteclan/models/user.dart';


part 'app_event.freezed.dart';

@freezed
abstract class AppEvent with _$AppEvent {
  const factory AppEvent.notAuthenticated() = _AppNotAuthenticatedEvent;

  const factory AppEvent.loading() = _AppLoadingEvent;

  const factory AppEvent.error(String msg) = _AppErrorEvent;

  const factory AppEvent.logging() = _AppLoggingEvent;

  const factory AppEvent.goToSwitchRoleScreen(User user) =
  _AppGoToSwitchRoleScreenEvent;

  const factory AppEvent.authenticated(User user) = _AppAuthenticatedEvent;

  const factory AppEvent.onboarded(User user) = _AppOnboardedEvent;

  const factory AppEvent.viewAsAdmin(User user) = _AppViewAsAdminEvent;


  const factory AppEvent.viewAsUser(User user) = _AppViewAsUserEvent;

  const factory AppEvent.viewAsSuperUser(User user) = _AppViewAsSuperUserEvent;

  const factory AppEvent.goToFingerPrintScreen(User user)=_AppGoToFingerPrintScreenEvent;


  const factory AppEvent.switchAccount(User user) = _AppSwitchAccountEvent;

  const factory AppEvent.checkAndSwitchAccount(User user) =
  _AppCheckAndSwitchAccountEvent;
}
