import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:graniteclan/models/user.dart';

import './bloc.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  @override
  AppState get initialState => AppState.started();
  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    yield* event.when(
      notAuthenticated: _mapNotAuthenticatedEventToState,
      loading: _mapToLoadingState,
      logging: _mapToLoggingState,
      goToFingerPrintScreen: _mapGoToFingerPrintScreen,
      goToSwitchRoleScreen: _mapGoToSwitchScreen,
      authenticated: _mapAuthenticatedEventToState,
      onboarded: _mapOnboardedEventToState,
      viewAsAdmin: _mapViewAsAdminEventToState,
      viewAsUser: _mapViewAsTraineeEventToState,
      viewAsSuperUser: _mapViewAsSuperUserEventToState,
      error: _mapNotErrorEventToState,
      switchAccount: _mapSwitchAccount,
      checkAndSwitchAccount: _mapCheckAndSwitchAccount,
    );
  }

  Stream<AppState> _mapNotAuthenticatedEventToState() async* {
    yield AppState.notAuthenticated(true, "nothing");
  }

  Stream<AppState> _mapNotErrorEventToState(msg) async* {
    yield AppState.showError(msg);
  }

  Stream<AppState> _mapGoToSwitchScreen(User user) async* {
    yield AppState.switchRole(user: user);
  }

  Stream<AppState> _mapToLoadingState() async* {
    yield state.maybeMap(
        notAuthenticated: (state) =>
            state.copyWith(isLogin: false, msg: "Loading"),
        orElse: () => state);
  }

  Stream<AppState> _mapToLoggingState() async* {
    yield state.maybeMap(
        notAuthenticated: (state) =>
            state.copyWith(isLogin: false, msg: "Logging In"),
        orElse: () => state);
  }

  Stream<AppState> _mapAuthenticatedEventToState(User user) async* {
    yield AppState.authenticated(user: user);
  }

  Stream<AppState> _mapOnboardedEventToState(User user) async* {
    yield AppState.admin(user: user);
  }

  Stream<AppState> _mapViewAsAdminEventToState(User user) async* {
    yield AppState.admin(user: user);
  }

  Stream<AppState> _mapViewAsTraineeEventToState(User user) async* {
    yield AppState.user(user: user);
  }

  Stream<AppState> _mapGoToFingerPrintScreen(User user) async* {
    yield AppState.fingerPrintAuthScreen(user: user);
  }

  Stream<AppState> _mapCheckAndSwitchAccount(User user) async* {
    if (state is AppState) {
      state.maybeMap(
          admin: (_) {
            if (!user.isAdmin) add(AppEvent.switchAccount(user));
          },
          user: (_) {
            if (!user.isUser) add(AppEvent.switchAccount(user));
          },
          superUser: (_) {
            if (!user.isSU) add(AppEvent.switchAccount(user));
          },
          orElse: () => state);
    }

  }

  Stream<AppState> _mapSwitchAccount(User user) async* {
    this.add(AppEvent.loading());

    if (user.isSU)
      this.add(AppEvent.viewAsSuperUser(user));
    else if (user.isAdmin)
      this.add(AppEvent.viewAsAdmin(user));
    else
      this.add(AppEvent.viewAsUser(user));
  }

  Stream<AppState> _mapViewAsSuperUserEventToState(User user) async* {
    yield AppState.superUser(user: user);
  }
}
