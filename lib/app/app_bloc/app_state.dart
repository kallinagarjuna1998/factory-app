import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:graniteclan/models/user.dart';

import 'package:meta/meta.dart';

part 'app_state.freezed.dart';

@freezed
abstract class AppState with _$AppState {
  const factory AppState.showError(String msg) = _AppShowErrorState;

  const factory AppState.started() = _AppStartedState;

  const factory AppState.notAuthenticated(bool isLogin, String msg) =
      _AppNotAuthenticatedState;

  const factory AppState.switchRole({@required User user}) =
      _AppSwitchRoleState;

  const factory AppState.authenticated({@required User user}) =
      _AppAuthenticatedState;

  const factory AppState.admin({@required User user}) = _AppAdminState;

  const factory AppState.user({@required User user}) = _AppUserState;

  const factory AppState.superUser({@required User user}) = _AppSuperUserState;

  const factory AppState.fingerPrintAuthScreen({@required User user}) =
      _AppFingerPrintAuthScreenState;
}
