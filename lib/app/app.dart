import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/providers/auth_provider.dart';
import 'package:graniteclan/router/router.gr.dart';
import 'package:graniteclan/services/auth_service.dart';
import 'package:graniteclan/services/user_service.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:provider/provider.dart';
import 'package:firebase_analytics/observer.dart';
import 'app_bloc/app_bloc.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
   FirebaseAnalytics analytics = FirebaseAnalytics();
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider<AuthService>(
            create: (context) => AuthService(),
          ),
          RepositoryProvider<UserService>(
            create: (context) => UserService(),
          )
        ],
        child: BlocProvider<AppBloc>(
          create: (_) => AppBloc(),
          child: MultiProvider(
            providers: [
              ChangeNotifierProvider<AuthProvider>(
                create: (context) => AuthProvider(
                  appBloc: BlocProvider.of<AppBloc>(context),
                  authService: AuthService(),
                ),
              ),
            ],
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'Granite Clan',
              theme: ThemeData(
                  primaryColor: const Color(0xFF1d0c98),
                  accentColor: Colors.blue[800],
                  //typography: Typography.material2020
                  ),
              home: ExtendedNavigator<Router>(
                router: Router(),
              ),
               navigatorObservers: [
                FirebaseAnalyticsObserver(analytics: analytics),
              ],
            ),
          ),
        ));
  }
}
