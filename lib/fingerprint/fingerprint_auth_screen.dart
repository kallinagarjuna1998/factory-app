import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/utils/fingerprint_class.dart';

class FingerPrintAuthScreen extends StatefulWidget {
  final User user;

  FingerPrintAuthScreen({@required this.user});

  @override
  _FingerPrintAuthScreenState createState() => _FingerPrintAuthScreenState();
}

class _FingerPrintAuthScreenState extends State<FingerPrintAuthScreen> {
  FingerprintAuth fingerprintAuth = FingerprintAuth();
  bool _isTrue = false;



  //Loading counter value on start
  _loadCounter(AppBloc appBloc) async {
   await fingerprintAuth.authenticate()?appBloc.add(AppEvent.switchAccount(widget.user)):null;
  }


  @override
  Widget build(BuildContext context) {
    final appBloc = BlocProvider.of<AppBloc>(context);
    _loadCounter(appBloc);
    return Scaffold(
      appBar: AppBar(
        title: Text("Fingerprint Auth Screen"),
      ),
      body: Stack(children: [
        Positioned(
          bottom: 20,
          left: 0,
          right: 0,
          child: SizedBox(
            width: double.infinity,
            height: 44,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: RaisedButton.icon(
                shape: StadiumBorder(),
                colorBrightness: Brightness.light,
                splashColor: Colors.white,
                icon: Icon(
                  Icons.fingerprint,
                  color: Colors.red,
                ),
                onPressed: () async => await fingerprintAuth.authenticate()
                    ? appBloc.add(AppEvent.switchAccount(widget.user))
                    : null,
                label: Text('Try Again', style: TextStyle(color: Colors.red)),
                color: Colors.white70,
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
