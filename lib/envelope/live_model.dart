import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:graniteclan/envelope/failure.dart';


part 'live_model.freezed.dart';

@freezed
abstract class LiveModel<T> with _$LiveModel<T> {
  const factory LiveModel.uninitialized() = _LiveModelUninitialized<T>;

  const factory LiveModel.loading() = _LiveModelLoading<T>;

  const factory LiveModel.loaded(T data) = _LiveModelLoaded<T>;

  const factory LiveModel.loadedEmpty() = _LiveModelLoadedEmpty<T>;

  const factory LiveModel.error(Failure failure) = _LiveModelError<T>;
}
