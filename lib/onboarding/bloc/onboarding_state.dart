import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:graniteclan/models/user.dart';

import 'package:meta/meta.dart';

part 'onboarding_state.freezed.dart';

@freezed
abstract class OnboardingState with _$OnboardingState {
  const factory OnboardingState.loading() = OnbordingLoadingState;

  const factory OnboardingState.notOnboarded(User user) = NotOnboardedState;
}
