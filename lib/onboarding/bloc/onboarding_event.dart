import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:graniteclan/models/user.dart';

import 'package:meta/meta.dart';

part 'onboarding_event.freezed.dart';

@freezed
abstract class OnboardingEvent with _$OnboardingEvent {
  const factory OnboardingEvent.addUser(User user) = _AddUserEvent;

  const factory OnboardingEvent.checkUserExists() = _CheckUserExistEvent;

  const factory OnboardingEvent.userExist(User user) = _UserExistsEvent;

  const factory OnboardingEvent.userNotExist() = _UserNotExistEvent;
}
