import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:graniteclan/app/app_bloc/app_bloc.dart';
import 'package:graniteclan/app/app_bloc/bloc.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/services/user_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './bloc.dart';

class OnboardingBloc extends Bloc<OnboardingEvent, OnboardingState> {
  final UserService userService;
  final AppBloc appBloc;
  final User user;
  StreamSubscription _userStreamSubscription;

  OnboardingBloc(
      {@required this.appBloc,
      @required this.user,
      @required this.userService}) {
    this.add(OnboardingEvent.checkUserExists());
  }

  Future<void> close() async {
    _userStreamSubscription?.cancel();

    //  _trainerStreamSubscription2.cancel();
    super.close();
  }

  @override
  OnboardingState get initialState => OnboardingState.loading();

  @override
  Stream<OnboardingState> mapEventToState(
    OnboardingEvent event,
  ) async* {
    yield* event.when(
      addUser: _mapAddUserEventToState,
      checkUserExists: _mapCheckUserExistEventToState,
      userExist: _mapUserExistsEventToState,
      userNotExist: _mapuserNotExistEventToState,
    );
  }

  Stream<OnboardingState> _mapAddUserEventToState(User entry) async* {
    yield OnboardingState.loading();
    final userAdded = await userService.addUser(entry);
    if (userAdded) {
      _mapCheckUserExistEventToState();
    } else {
      yield OnboardingState.notOnboarded(user);
    }
  }

  Stream<OnboardingState> _mapCheckUserExistEventToState() async* {
    if (_userStreamSubscription == null) {
      _userStreamSubscription =
          userService.getUser(user.uid).listen((newUser) async {
        if (newUser != null) {
          SharedPreferences prefs = await SharedPreferences.getInstance();

          if (!(prefs.getBool('isFingerprintAuth') ?? false)) {
            this.add(OnboardingEvent.userExist(newUser));
          } else
            appBloc.add(AppEvent.goToFingerPrintScreen(newUser));
        } else {
          this.add(OnboardingEvent.userNotExist());
        }
      });
    }
  }

  Stream<OnboardingState> _mapUserExistsEventToState(User user) async* {
    //yield OnboardingState.onboarded(user);
    appBloc.add(AppEvent.switchAccount(user));
  }

  Stream<OnboardingState> _mapuserNotExistEventToState() async* {
    yield OnboardingState.notOnboarded(user);
  }
}
