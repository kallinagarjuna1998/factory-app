import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/login/login_screen.dart';
import 'package:graniteclan/onboarding/bloc/bloc.dart';
import 'package:graniteclan/widgets/notification_button.dart';
import 'package:graniteclan/widgets/user_profile_form.dart';

class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final FirebaseMessaging _fcm = FirebaseMessaging();

  Widget _buildOnboardingForm(NotOnboardedState state) {
    final user = state.user;

    return Scaffold(
      appBar: AppBar(
        title: Text('Onboarding Screen'),
        actions: <Widget>[
          NotificationButton(),
        ],
      ),
      body: UserProfileForm(
        user: user,
        isEdit: false,
        onSaveCallback: (user) async {
          final fcmToken = await _fcm.getToken();
          _fcm.subscribeToTopic("AllUsers");
          user = user.copyWith(
            registeredTime: DateTime.now(),
            isUser: true,
            isSU: false,
            isAdmin: false,
            fcmToken: fcmToken,
          );
          BlocProvider.of<OnboardingBloc>(context)
              .add(OnboardingEvent.addUser(user));
        },
      ),
    );
  }

  Widget _buildLoadingWidget(OnbordingLoadingState state) {
    /* final appBloc = BlocProvider.of<AppBloc>(context);
    appBloc.add(AppEvent.logging());*/
    return LoginScreen(
      isLogin: false,
      msg: "Logging In",
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<OnboardingBloc, OnboardingState>(
          builder: (BuildContext context, OnboardingState state) {
        return state.map(
          loading: _buildLoadingWidget,
          notOnboarded: _buildOnboardingForm,
        );
      }),
    );
  }
}
