import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:graniteclan/fcm_notifications/models/notification.dart';
import 'package:graniteclan/services/firebase_model_extensions.dart';

class NotificationService {
  Firestore _firestore;
  static const keyNotifications = "Notifications";

  NotificationService() : _firestore = Firestore.instance;

  Stream<FcmNotificationList> getAllUsers() {
    return _firestore
        .collection(keyNotifications)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      final List<FcmNotification> userList = snapshot.documents
          .map((DocumentSnapshot document) => document.asFcmNotification())
          .toList();

      return FcmNotificationList(userList);
    });
  }

  Future<void> addNotification(FcmNotification notification) async {
    final notificationRef = _firestore.collection(keyNotifications);
    final documentId = notification.id ?? notificationRef.document().documentID;
    final data = notification.toJson();
    try {
      await notificationRef.document(documentId).setData(data);
      return true;
    } catch (e) {
      return false;
    }
  }
}
