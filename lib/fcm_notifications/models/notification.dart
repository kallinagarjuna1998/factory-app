import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'notification.freezed.dart';

part 'notification.g.dart';

@freezed
abstract class FcmNotification with _$FcmNotification {
  factory FcmNotification({
    String id,
    String title,
    String messageBody,
    String topicName,
    String userId,
  }) = _Notification;

  factory FcmNotification.fromJson(Map<String, dynamic> json) =>
      _$FcmNotificationFromJson(json);
}

@freezed
abstract class FcmNotificationList with _$FcmNotificationList {
  factory FcmNotificationList(List<FcmNotification> list) = _FcmNotificationList;

  @late
  bool get isEmpty => list.isEmpty;

  @late
  bool get isNotEmpty => list.isNotEmpty;
}
