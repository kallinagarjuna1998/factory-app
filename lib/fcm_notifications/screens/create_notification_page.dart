import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:graniteclan/fcm_notifications/models/notification.dart';
import 'package:graniteclan/fcm_notifications/provider/notification_form_provider.dart';
import 'package:graniteclan/fcm_notifications/services/notification_service.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/widgets/diet_ondone_button.dart';
import 'package:provider/provider.dart';

class CreateTopicNotification extends StatefulWidget implements AutoRouteWrapper {
  final User user;

  CreateTopicNotification({Key key, this.user}) : super(key: key);

  @override
  _CreateTopicNotificationState createState() => _CreateTopicNotificationState();

  @override
  Widget wrappedRoute(BuildContext context) {
    // TODO: implement wrappedRoute
    return ChangeNotifierProvider<NotificationFormProvider>(
      create: (context) => NotificationFormProvider(
        notificationService: NotificationService(),
      ),
      child: this,
    );
  }
}

class _CreateTopicNotificationState extends State<CreateTopicNotification> {
  final GlobalKey<FormBuilderState> _peKey = GlobalKey<FormBuilderState>();

  Future<void> _validateAndSave(
      NotificationFormProvider notificationFormProvider) async {
    if (_peKey.currentState.saveAndValidate()) {
      FcmNotification entry =
          FcmNotification.fromJson(_peKey.currentState.value);
      notificationFormProvider
          .addNotification(entry.copyWith(userId: widget.user.uid));
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    NotificationFormProvider notificationFormProvider =
        Provider.of<NotificationFormProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Topic Notifications"),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: SingleChildScrollView(
              child: FormBuilder(
                key: _peKey,
                //   initialValue: widget.priceEntry.toJson(),
                autovalidate: true,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: FormBuilderTextField(
                        attribute: "title",
                        decoration: InputDecoration(
                            hintText: "Notification Title", labelText: "Title"),
                        validators: [FormBuilderValidators.required()],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: FormBuilderTextField(
                        attribute: "messageBody",
                        decoration: InputDecoration(
                            hintText: "Message body",
                            labelText: "Message Body"),
                        validators: [FormBuilderValidators.required()],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: FormBuilderDropdown(
                        attribute: 'topicName',
                        decoration:
                            InputDecoration(labelText: "Select Topic Name"),
                        hint: Text("Topic Name"),
                        validators: [FormBuilderValidators.required()],
                        items: ["AllUsers", "Gyms", "Trainers"].map((options) {
                          return DropdownMenuItem(
                            child: Text(options),
                            value: options,
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5),
            child: OnDoneButton(
              text: "Save",
              onPressed: () {
                _validateAndSave(notificationFormProvider);
              },
            ),
          ),
        ],
      ),
    );
  }
}
