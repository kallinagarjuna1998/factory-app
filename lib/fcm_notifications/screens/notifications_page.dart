import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    _fcm.configure(onMessage: (Map<String, dynamic> message) async {
      print("onMessage:$message");
      final snackBar = SnackBar(
        content: Text(message["notification"]["title"]),
        action: SnackBarAction(
          label: "Go",
          onPressed: () => null,
        ),
      );
    }, onResume: (Map<String, dynamic> message) async {
      print("onResume:$message");
    }, onLaunch: (Map<String, dynamic> message) async {
      print("onLaunch:$message");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("NOTIFICATIONS"),
      ),
      body: Column(
        children: <Widget>[Text("Notifications")],
      ),
    );
  }
}
