import 'package:flutter/material.dart';
import 'package:graniteclan/fcm_notifications/models/notification.dart';
import 'package:graniteclan/fcm_notifications/services/notification_service.dart';

class NotificationFormProvider with ChangeNotifier {
  FcmNotification fcmNotification;
  NotificationService notificationService;

  NotificationFormProvider(
      {@required this.fcmNotification, @required this.notificationService}) {
    if (fcmNotification == null) fcmNotification = FcmNotification(topicName: "Pounds");
  }

  addNotification(FcmNotification fcmNotification) {
    notificationService.addNotification(fcmNotification);
  }
}
