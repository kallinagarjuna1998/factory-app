import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:graniteclan/providers/auth_provider.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatefulWidget {
  final bool isLogin;
  final String msg;

  LoginScreen({@required this.isLogin, @required this.msg});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _key = GlobalKey<ScaffoldState>();
  Color bgColor = Colors.blue[900];

  @override
  void initState() {
    super.initState();
  }

  void onPageChanged(int pageNum) {
    setState(() {
      switch (pageNum) {
        case 0:
          bgColor = Colors.blue[900];
          break;
        case 1:
          bgColor = Colors.green[700];
          break;
        case 2:
          bgColor = Colors.yellow[900];
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      body: Container(
        padding: MediaQuery.of(context).padding,
        width: double.infinity,
        height: double.infinity,
        color: Theme.of(context).primaryColor,
        child: Consumer<AuthProvider>(
          builder: (_, authProvider, __) {
            return Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                                height: 150,
                                width: 150,
                                child: Image.asset('assets/icon/new_logo.png')),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text('Venom',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500)),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 120)
                    ],
                  ),
                ),
                Visibility(
                  key: ValueKey("Nothing"),
                  visible: widget.isLogin ?? true,
                  child: Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          width: double.infinity,
                          height: 44,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: RaisedButton.icon(
                              shape: StadiumBorder(),
                              colorBrightness: Brightness.light,
                              splashColor: Colors.white,
                              icon: Icon(MdiIcons.google, color: Colors.red[800]),
                              onPressed: () => authProvider.signInWithGoogle(),
                              label: Text('JOIN WITH GOOGLE',
                                  style: TextStyle(color: Colors.red[800])),
                              color: Colors.white70,
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Center(
                            child: RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                    text: 'By continuing, you agree to our\n',
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.white)),
                                TextSpan(
                                    text: 'Terms of Service',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12.0,
                                      decoration: TextDecoration.underline,
                                    ),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        launch("https://fitpaa.com/terms.html");
                                      })
                              ]),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Visibility(
                  key: ValueKey("loading"),
                  visible: (!widget.isLogin) ?? false,
                  child: Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      height: 90,
                      alignment: Alignment.topCenter,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.white)),
                            SizedBox(width: 20),
                            Text(widget.msg ?? "Nothing",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 24))
                          ]),
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
