import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:graniteclan/widgets/diet_ondone_button.dart';
import 'package:provider/provider.dart';

class EditUserRole extends StatefulWidget implements AutoRouteWrapper {
  final UserProvider userProvider;

  EditUserRole({@required this.userProvider});

  @override
  _EditUserRoleState createState() => _EditUserRoleState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return ChangeNotifierProvider<UserProvider>.value(
      value: userProvider,
      child: this,
    );
  }
}

class _EditUserRoleState extends State<EditUserRole> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  _validateAndSave(User user) async {
    if (_fbKey.currentState.saveAndValidate()) {
      print(_fbKey.currentState.value);
      Map<String, dynamic> entryData = _fbKey.currentState.value;
      Provider.of<UserProvider>(context, listen: false)
          .updateRoleData(data: entryData);
      Navigator.pop(context);
    }
  }

  String _validateRoles(_) {
    final values = _fbKey.currentState.value?.values;

    final isValid = values
            ?.map((e) => e as bool)
            ?.fold(false, (bool valid, bool value) => valid || value) ??
        false;

    return !isValid ? "Please select atleast one role" : null;
  }

  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    User user = userProvider.user;

    return Scaffold(
        appBar: AppBar(
          title: Text("Edit User Role"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                FormBuilder(
                  key: _fbKey,
                  autovalidate: true,
                  initialValue: user.roleData,
                  child: Column(
                    children: <Widget>[
                      FormBuilderSwitch(
                        attribute: "isUser",
                        //  initialValue: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                        ),
                        label: Text("Is User"),
                        onChanged: (value) {
                          user = user.copyWith(isUser: value);
                          print(user.isUser);
                        },
                        validators: [
                          _validateRoles
                          //  FormBuilderValidators.r
                        ],
                      ),
                      FormBuilderSwitch(
                        attribute: "isAdmin",
                        //    initialValue: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                        ),
                        label: Text("is Admin"),
                        onChanged: (value) {
                          user = user.copyWith(isAdmin: value);
                        },
                        validators: [
                          _validateRoles
                          //  FormBuilderValidators.r
                        ],
                      ),
                      FormBuilderSwitch(
                        attribute: "isSU",
                        //   initialValue: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                        ),
                        label: Text("is SuperUser"),
                        onChanged: (value) {
                          user = user.copyWith(isSU: value);
                        },
                        validators: [
                          _validateRoles
                          //  FormBuilderValidators.r
                        ],
                      ),
                    ],
                  ),
                ),
                OnDoneButton(
                  text: "Save",
                  onPressed: () {
                    _validateAndSave(user);
                  },
                )
              ],
            ),
          ),
        ));
  }
}
