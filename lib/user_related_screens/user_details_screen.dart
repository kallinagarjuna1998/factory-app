import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:graniteclan/router/router.gr.dart';
import 'package:graniteclan/services/user_service.dart';
import 'package:graniteclan/widgets/circle_text_avatar.dart';
import 'package:graniteclan/widgets/url_launcher.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

class UserDetailsScreen extends StatelessWidget implements AutoRouteWrapper {
  final User user;
  final toShowEndDrawer;

  UserDetailsScreen({@required this.user, this.toShowEndDrawer: true});

  @override
  Widget wrappedRoute(BuildContext context) {
    return ChangeNotifierProvider<UserProvider>(
      create: (context) => UserProvider(
        user: user,
        userService: RepositoryProvider.of(context),
        //  appBloc: BlocProvider.of(context),
      ),
      child: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    String gender;
    UrlLauncher launcher = UrlLauncher();
    final user = userProvider.user;
    if (user.gender == 1) {
      gender = "Male";
    } else
      gender = "Female";
    return Scaffold(
      appBar: AppBar(
        title: Text("User Details"),
      ),
      endDrawer: toShowEndDrawer
          ? Drawer(
              child: ListView(
                children: <Widget>[
                  Divider(height: 0.5, color: Colors.black26),
                  ListTile(
                    leading: Icon(
                      Icons.edit,
                      color: Colors.black,
                    ),
                    title: Text("Edit Your Details"),
                    trailing: Icon(Icons.navigate_next),
                    onTap: () {
                      ExtendedNavigator.of(context)
                          .pushNamed(Routes.editUserDetails,
                              arguments: EditUserDetailsArguments(
                                userProvider: userProvider,
                              ));
                    },
                  ),
                  Divider(height: 0.5, color: Colors.black26),
                  ListTile(
                    leading: Icon(
                      Icons.edit,
                      color: Colors.black,
                    ),
                    title: Text("Edit Your Role"),
                    trailing: Icon(Icons.navigate_next),
                    onTap: () {
                      ExtendedNavigator.of(context)
                          .pushNamed(Routes.editUserRole,
                              arguments: EditUserRoleArguments(
                                userProvider: userProvider,
                              ));
                    },
                  ),
                  Divider(height: 0.5, color: Colors.black26),

                  //  traineeOptions(user, context),
                  //  trainerOptions(user, context),
                  //   adminOptions(user),
                ],
              ),
            )
          : null,
      body: Column(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.all(10),
              child: CachedNetworkImage(
                imageUrl: user.photoUrl,
                // fit: BoxFit.cover,

                imageBuilder: (context, imageProvider) => CircleAvatar(
                  backgroundImage: imageProvider,
                  radius: 50,
                ),
                placeholder: (context, url) =>
                    CircularTextAvatar(name: user.name),
                errorWidget: (context, url, error) =>
                    CircularTextAvatar(name: user.name),
              )),
          Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                user.name,
                style: TextStyle(fontSize: 24),
              )),
          ListTile(
            title: Text("Email"),
            subtitle: Text(user.email),
            leading: SizedBox(
              height: 30,
              width: 30,
              child: IconButton(
                icon: Icon(Icons.email),
                color: Colors.black,
                onPressed: () {
                  launcher.email(user.email);
                },
              ),
            ),
          ),
          ListTile(
            title: Text("Phone Number"),
            subtitle: Text(user.phoneNumber),
            leading: SizedBox(
              height: 30,
              width: 30,
              child: IconButton(
                icon: Icon(Icons.call),
                color: Colors.green,
                onPressed: () {
                  launcher.call(user.phoneNumber);
                },
              ),
            ),
            trailing: IconButton(
              icon: Icon(
                MdiIcons.whatsapp,
                color: Colors.green,
                size: 30,
              ),
              // color: Colors.green,
              onPressed: () {
                launcher.whatsApp(user.phoneNumber);
              },
            ),
          ),
          ListTile(
            title: Text("Age", style: Theme.of(context).textTheme.body2),
            subtitle: Text(user.age.toString(),
                style: Theme.of(context).textTheme.body1),
            leading: SizedBox(
                height: 30,
                width: 30,
                child: Icon(
                  Icons.account_box,
                  color: Colors.black,
                )),
          ),
          ListTile(
            title: Text("Gender", style: Theme.of(context).textTheme.body2),
            subtitle: Text(gender, style: Theme.of(context).textTheme.body1),
            leading: SizedBox(
                height: 30,
                width: 30,
                child: Icon(
                  user.gender == 1
                      ? MdiIcons.genderMale
                      : MdiIcons.genderFemale,
                  color: Colors.black,
                )),
          ),
        ],
      ),
    );
  }
}
