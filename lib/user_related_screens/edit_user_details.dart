import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:graniteclan/models/user.dart';
import 'package:graniteclan/providers/user_provider.dart';
import 'package:graniteclan/widgets/user_profile_form.dart';
import 'package:provider/provider.dart';

class EditUserDetails extends StatefulWidget implements AutoRouteWrapper {
  final UserProvider userProvider;

  EditUserDetails({@required this.userProvider});

  @override
  _EditUserDetailsState createState() => _EditUserDetailsState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return ChangeNotifierProvider<UserProvider>.value(
      value: userProvider,
      child: this,
    );
  }
}

class _EditUserDetailsState extends State<EditUserDetails> {
  @override
  Widget build(BuildContext context) {
    // final user = Provider.of<User>(context);
    final userProvider = Provider.of<UserProvider>(context);
    final user = userProvider.user;
    return Scaffold(
        appBar: AppBar(
          title: Text("My Profile"),
        ),
        body: ChangeNotifierProvider<UserProvider>.value(
          value: userProvider,
          child: UserProfileForm(
            user: user,
            isEdit: false,
            onSaveCallback: _editAndSave,
          ),
        ));
  }

  _editAndSave(User user) async {
    Provider.of<UserProvider>(context, listen: false)
        .updateUser(user: user, uid: user.uid);
    Navigator.pop(context);
  }
}
