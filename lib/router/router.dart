import 'package:auto_route/auto_route_annotations.dart';
import 'package:graniteclan/app/home_screen.dart';
import 'package:graniteclan/screens/about_us.dart';
import 'package:graniteclan/fcm_notifications/screens/create_notification_page.dart';
import 'package:graniteclan/fcm_notifications/screens/notifications_page.dart';
import 'package:graniteclan/user_related_screens/edit_user_details.dart';
import 'package:graniteclan/user_related_screens/edit_user_role.dart';
import 'package:graniteclan/user_related_screens/user_details_screen.dart';

@MaterialAutoRouter()
class $Router {
  @initial
  HomeScreen initialPage;
  NotificationsPage notificationsPage;
  AboutUsPage aboutUsPage;
  EditUserDetails editUserDetails;
  EditUserRole editUserRole;
  UserDetailsScreen userDetailsScreen;
  CreateTopicNotification createTopicNotification;
}
